export interface FilePickerResult {
  bucket?: string;
  target: string;
  files?: string[];
}
