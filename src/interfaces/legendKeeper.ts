export interface LKEntry {
  schemaVersion: number;
  id: string;
  name: string;
  glyph: string;
  pinShape: string;
  createdAt: string;
  createdBy: CreatedBy;
  image?: null;
  imageOrigin?: null[] | null;
  imageScale?: null;
  parentId: string;
  tags?: null[] | null;
  documents?: DocumentsEntity[] | null | undefined;
  rootLocale?: RootLocale;
  children?: LKEntry[];
}

export interface CreatedBy {
  id: string;
  name: string;
}

export interface DocumentsEntity {
  id: string;
  name?: null;
  version: number;
  content: string;
}

export interface RootLocale {
  id: string;
}

export interface LKEntryContainer {
  resources: LKEntry[];
}
