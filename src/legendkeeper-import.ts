import { registerSettings } from "./module/settings.js";
import { preloadTemplates } from "./module/preloadTemplates.js";
import { FilePickerResult } from "./interfaces/foundryTypes";
import { LKEntry, LKEntryContainer } from "./interfaces/legendKeeper";

let maxDialogValue = 0;
let currDialogValue = 1;

/* ------------------------------------ */
/* Initialize module					          */
/* ------------------------------------ */
Hooks.once("init", async function () {
  console.log("legendkeeper-import | Initializing legendkeeper-import");

  // Assign custom classes and constants here

  // Register custom module settings
  registerSettings();

  // Preload Handlebars templates
  await preloadTemplates();

  // Register custom sheets (if any)
});

/* ------------------------------------ */
/* Setup module							            */
/* ------------------------------------ */
Hooks.once("setup", function () {
  // Do anything after initialization but before
  // ready
});

/* ------------------------------------ */
/* When ready							              */
/* ------------------------------------ */
Hooks.once("ready", function () {
  // Do anything once the module is ready
  console.log("Legend Keeper Import Ready");
});

// Add any additional hooks if necessary

Hooks.on("renderJournalDirectory", async (app, html, options, id) => {
  // Update the LegendKeeper imported entries so they have the LK icon
  html.find(".folder h3, .journal h4").each((index, element) => {
    let $self = $(element);
    let content = $self.html();

    if (content.includes("[LK]")) {
      content = content.replace(/\[LK\]/g, '<span class="lk-icon"></span>');

      $self.html(content);
    }
  });

  // If the user is a GM attach the button to the bottom of the jounal
  // entries sidebar
  if (!game.user.isGM) return;

  const button = $(`<button type="button" id="lk-import">
    ${game.i18n.localize("LK_IMPORT.buttons.import")}
  		</button>`);
  button.on("click", async (ev) => {
    const entriesPromise = await getAllEntries();

    const tree = createTree(entriesPromise.resources);

    const stats = {
      itemsCreated: 0,
      itemsUpdated: 0,
      foldersCreated: 0,
      foldersUpdated: 0,
    };

    await processTree(tree, null, stats);

    console.info(stats);

    await fixLinks();
    await deleteRemovedEntries(entriesPromise.resources);
  });

  html.find(".directory-footer").append(button);
});

/**
 * Take a file path, get the file in question and then convert it to JSON so it
 * matches the definition of an entry from Legenf Keeper.
 *
 * @param filePath The path to the file to process
 * @returns An LKEntry object for the processed file
 */
async function getJsonForFile(
  filePath: string
): Promise<LKEntryContainer> | null {
  const response = await fetch(filePath);

  if (response.status !== 200) {
    return null;
  } else {
    const json = await response.json();
    return json;
  }
}

/**
 * Using the path in the settings for Legend Keeper Import, get all the files,
 * convert them to LKEntry objects and then return them.
 *
 * @returns An array of promises that resolve to LKEntry objects
 */
async function getAllEntries(): Promise<LKEntryContainer> {
  console.info("LK Import: Getting all entries to process.");

  renderProgress(false);

  const file = String(game.settings.get("legendkeeper-import", "importFile"));

  const fileEntries = await getJsonForFile(file);

  maxDialogValue = fileEntries.resources.length;

  return fileEntries;
}

async function deleteRemovedEntries(entries: LKEntry[]) {
  console.info(
    "LK Import: Deleting journal entries no longer in Legend Keeper."
  );

  const existingJournalEntries = game.journal.filter(
    (j) => j.data.flags.legendKeeperId != undefined
  );

  existingJournalEntries.forEach(async (eje) => {
    const entryExists =
      entries.filter((f) => f.id === eje.data.flags.legendKeeperId).length > 0;

    if (!entryExists) {
      console.log(`Legend Keeper entry for ${eje.name} has been deleted, \
                   therefore removing journal entry.`);
      await eje.delete();
    }
  });
}

/**
 * Get all the children of LKEntry objects in the passed in array so that we
 * we have a tree of all the entries that we want to import into Foundry
 *
 * @param entries The array of LKEntry objects to find all children for
 * @returns An array of LKEntry objects now in a tree format
 */
function createTree(entries: LKEntry[]): LKEntry[] {
  console.info("LK Import: Creating tree of entries.");
  const returnArray: Array<LKEntry> = [];
  const topItems = entries.filter((item) => !item.parentId && !item.rootLocale);

  topItems.forEach((i) => {
    i.children = getChildren(i, entries);
    returnArray.push(i);
  });

  return returnArray;
}

/**
 * Process a tree of LKEntries and create the folders and journal entries for
 * them.
 *
 * If an item would end up more than 3 levels deep, then it is moved up to the
 * grandparent as Foundry doesn't support > 3 levels depth in folders.
 *
 * @param tree The tree of LKEntry items to process and turn into folders and
 *     Journal Entries
 * @param parent The parent folder to attach this tree to
 */
async function processTree(
  tree: LKEntry[],
  parent: Folder | null,
  stats: {
    itemsCreated: number;
    itemsUpdated: number;
    foldersCreated: number;
    foldersUpdated: number;
  }
) {
  if (parent) {
    console.info("LK Import: Processing child tree of entries.");
  } else {
    console.info("LK Import: Processing tree of entries.");
  }

  const existingJournalFolders = game.folders.filter(
    (j) => j.data.flags.legendKeeperId != undefined && j.type === "JournalEntry"
  );

  for (const item of tree) {
    if (item.children.length > 0) {
      let newParent: DeepPartial<Folder> | null = null;

      if (parent) {
        newParent = {
          id: parent.id,
          link: parent.link,
          entity: parent.entity,
          type: parent.type,
          uuid: parent.uuid,
          _id: parent._id,
        };
      }

      if (parent && parent["depth"] === 3) {
        console.info(
          `LK Import: Item: ${item.name}'s parent is too deep so assigning to grandparent.`
        );

        const nextParent = game.folders.filter(
          (f) => f.id === String(parent.data.parent)
        );

        if (nextParent.length > 0) {
          newParent = {
            id: nextParent[0].id,
            link: nextParent[0].link,
            entity: nextParent[0].entity,
            type: nextParent[0].type,
            uuid: nextParent[0].uuid,
            _id: nextParent[0]._id,
          };
        }
      }

      let folderToUse: Folder = null;

      const existingFolder = existingJournalFolders.filter(
        (f) => f.data.flags.legendKeeperId === item.id
      );

      const newParentId = newParent != null ? newParent.id : null;

      if (existingFolder.length > 0) {
        stats.foldersUpdated++;

        await existingFolder[0].update({
          _id: existingFolder[0].id,
          name: `[LK] ${item.name}`,
          parent: newParentId,
        });

        folderToUse = existingFolder[0];
      } else {
        stats.foldersCreated++;

        const folder = await Folder.create({
          name: `[LK] ${item.name}`,
          type: "JournalEntry",
          parent: newParentId,
          "flags.legendKeeperId": item.id,
        });

        folderToUse = folder;
      }

      if (item.documents.length > 0) {
        await createJournalEntry(item, folderToUse.id, stats);
      }

      await processTree(item.children, folderToUse, stats);
    } else {
      if (item.documents.length > 0) {
        await createJournalEntry(item, parent?.id, stats);
      }
    }

    currDialogValue += 1;
    renderProgress(true);
  }
}

/**
 * Create a journal entry in a folder based on the LKEntry object passed in.
 *
 * @param entry The LKEntry object to use to create the journal entry
 * @param folderId The id of the folder to generate the entry in
 */
async function createJournalEntry(
  entry: LKEntry,
  folderId: string,
  stats: {
    itemsCreated: number;
    itemsUpdated: number;
    foldersCreated: number;
    foldersUpdated: number;
  }
) {
  const htmlTagRegex = /<(?!\/>)[^>]*>/gi;
  const inner = entry.documents[0].content.replace(htmlTagRegex, "").trim();

  const existingJournalEntries = game.journal.filter(
    (j) => j.data.flags.legendKeeperId != undefined
  );

  if (inner !== "") {
    let entryContent = "";

    entry.documents.forEach((d) => {
      if (d.name) {
        entryContent += `<h2>${d.name}</h2>`;
      }
      entryContent += parseContent(d.content);
    });

    const journalEntryData = {
      name: `[LK] ${entry.name}`,
      folder: folderId,
      content: entryContent,
      flags: {
        legendKeeperId: entry.id,
      },
    };

    const existingEntry = existingJournalEntries.filter(
      (f) => f.data.flags.legendKeeperId === entry.id
    );

    if (existingEntry.length > 0) {
      stats.itemsUpdated++;
      await existingEntry[0].update(journalEntryData);
    } else {
      stats.itemsCreated++;
      await JournalEntry.create(journalEntryData);
    }
  }
}

async function fixLinks() {
  // Need to turn
  // <a href=\"ckmkctp2ixoip076281319ijb.html\">Temple of the Restful Lily</a>
  // into
  // @JournalEntry[SKOWo9Ui6tzNmLRC]{Temple of the Restful Lily}

  console.info("LK Import: Fixing links in existing entries.");

  const linkRegex = new RegExp(
    '<a href=\\"([a-zA-z0-9]*?).html\\">(.*?)</a>',
    "gim"
  );

  const existingJournalEntries = game.journal.filter(
    (j) => j.data.flags.legendKeeperId != undefined
  );

  existingJournalEntries.forEach(async (e) => {
    let match;

    while ((match = linkRegex.exec(e.data.content)) !== null) {
      if (match.length > 2) {
        const matchingEntry = game.journal.filter(
          (f) => f.data.flags.legendKeeperId === match[1]
        );

        if (matchingEntry.length > 0) {
          const replaceString = e.data.content.replace(
            match[0],
            `@JournalEntry[${matchingEntry[0].id}]{${match[2]}}`
          );

          const journalEntryData = { ...e.data };

          journalEntryData.content = replaceString;

          await e.update(journalEntryData);
        }
      }
    }
  });
}

/**
 * This function takes a string and modifies the content so that the following
 * items are dealt with:
 *
 * - Secret sections
 *
 * @param content The string to parse and modify content for
 * @returns A string with the modifications performed
 */
function parseContent(content: string): string {
  const secretRegex =
    /<div data-node-type="bodied-extension" data-extension-type="com\.algorific\.legendkeeper\.extensions" data-extension-key="block-secret".*?>(.*?)<\/div>/gim;

  const returnString = content.replace(
    secretRegex,
    '<p><section class="secret">$1</section></p>'
  );

  return returnString;
}

/**
 * Recursively get children for LKEntry objects so that we have a full tree.
 *
 * @param parent The item to get children for
 * @param allEntries The array of all LKEntry objects to check for children
 * @returns An array of LKEntry objects
 */
function getChildren(parent: LKEntry, allEntries: LKEntry[]): LKEntry[] {
  const returnArray = allEntries.filter((item) => item.parentId === parent.id);

  returnArray.forEach((ra) => {
    ra.children = getChildren(ra, allEntries);
  });

  return returnArray;
}

/**
 * Create/update the dialog that shows the overall progress of the Legend
 * Keeper Wiki import process.
 *
 * @param update If set to true the dialog will be updated instead of created
 */
function renderProgress(update: boolean) {
  if (!update) {
    const d = new Dialog({
      title: "Legend Keeper Import",
      content: `<br/><br/><p class="lkMessage">Importing Legend Keeper Wiki</p>
                <div class="meter">
                  <span class="progress-meter" style="width: 25%"></span>
                </div>`,
      buttons: {
        close: {
          icon: '<i class="fas fa-check"></i>',
          label: "Close",
        },
      },
      default: "close",
      close: (html) => (currDialogValue = 0),
    });
    d.render(true);
  } else {
    if (currDialogValue === maxDialogValue) {
      $(document)
        .find(".lkMessage")
        .text(`Legend Keeper Wiki import completed`);
    }
    $(document)
      .find(".progress-meter")
      .css("width", `${(currDialogValue / maxDialogValue) * 100}%`);
  }
}
