export const registerSettings = function () {
    game.settings.register("legendkeeper-import", "importFile", {
        name: "LK_IMPORT.importFile.name",
        hint: "LK_IMPORT.importFile.hint",
        scope: "world",
        config: true,
        type: String,
        default: "",
    });
};

//# sourceMappingURL=settings.js.map
